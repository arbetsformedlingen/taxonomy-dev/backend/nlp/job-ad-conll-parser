(ns jobtech.job-ad-conll-parser
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.java.shell :as sh]
            [msync.lucene :as lucene]
            [msync.lucene [document :as ld]]
            [msync.lucene.analyzers :as analyzers]
            [clojure.data.json :as json]
            [msync.lucene.indexer :as indexer]
            [iota]
            [clojure.set :as s]
            [clojure.tools.logging :as log]
            [clojure.walk :as w]
            )
  (:import org.apache.lucene.index.DirectoryReader
           org.apache.lucene.index.TermsEnum
           )
  (:gen-class))


(comment
""

  )



(def micro1-ads-file "/home/jafhk/repo/data/conll/pb_2006_2020/micro1.jsonl")
(def micro2-ads-file "/home/jafhk/repo/data/conll/pb_2006_2020/micro2.jsonl")

(def all-ads-file "/home/jafhk/repo/data/conll/pb_2006_2020/all.jsonl")
(def jobads-file "/home/jafhk/repo/data/conll/2016.jsonl")

(defn greet
  "Callable entry point to the application."
  [data]
  (println (str "Hello, " (or (:name data) "World") "!")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (greet {:name (first args)}))

(defn read-conll []
   (with-open [reader (io/reader "resources/jobads.conll")]
       (doall
        (csv/read-csv reader :separator \tab ))))


;; (sh/sh "grep" "vinterpalatset" jobads-file)



;; (json/read-str (slurp "resources/jobad2.json") :key-fn keyword)

(def load-ad (json/read-str (slurp "resources/jobad2.json") :key-fn keyword))

(defn parse-ad [data]
  {:text (get-in data [:description :text])
   :headline (:headline data)
   }
  )

;; In the common namespace
;; This is the default analyzer, an instance of the StandardAnalyzer
;; of Lucene
(defonce default-analyzer (analyzers/standard-analyzer))

;; This analyzer considers field values verbatim
;; Will not tokenize and stem
(defonce keyword-analyzer (analyzers/keyword-analyzer))

(defonce ad-index (lucene/create-index! :type :memory
                                        :analyzer default-analyzer))


;; (lucene/index! ad-index [(parse-ad load-ad)] {:stored-fields [:text :headline]})



(def ad-index-path "/home/jafhk/repo/data/conll/ad-index/")
(defonce ad-index-disk (lucene/create-index! :type :disk
                                             :path ad-index-path
                                             :analyzer default-analyzer))

(def ad-index-path2 "/home/jafhk/repo/data/conll/ad-index2/")
(defonce ad-index-disk2 (lucene/create-index! :type :disk
                                             :path ad-index-path2
                                             :analyzer default-analyzer))


(def ad-index-path-all "/home/jafhk/repo/data/conll/ad-index-all")
(defonce ad-index-all (lucene/create-index! :type :disk
                                           :path ad-index-path-all
                                              :analyzer default-analyzer))

(def micro-index-path "/home/jafhk/repo/data/conll/micro-index")
(defonce micro-index (lucene/create-index! :type :disk
                                           :path micro-index-path
                                            :analyzer default-analyzer))


;; (lucene/index! ad-index-disk [(parse-ad load-ad)] {:stored-fields [:text :headline]})
;; (lucene/search ad-index {:text "organisation"} {:results-per-page 1 :hit->doc ld/document->map})

;; (def jobads-2016 (iota/numbered-vec jobads-file))
(def jobads-2016 (iota/seq jobads-file))
(def jobads-2016-data (map #(json/read-str % :key-fn keyword) jobads-2016))



;; (lucene/index! ad-index-disk jobads-2016-data {:stored-fields [:text :headline]})

;; (lucene/index! ad-index-disk (doall jobads-2016-data) {:stored-fields [:access]})


;; (lucene/index! ad-index-disk (doall jobads-2016-data) {:stored-fields [:access]})
;; /home/jafhk/repo/data/conll

(defn prefix-keys-fn [prefix]
  (fn [acc element]
    (assoc acc element (keyword (str (name prefix) "_" (name element))) ))
  )

(defn prefix-keys [k m]
  (s/rename-keys m (reduce (prefix-keys-fn k) {} (keys m)))
  )

(defn flatten-key [m k]
  (-> m
      (merge (prefix-keys k (get m k)))
      (dissoc k)
      ))

#_(defn flatten-keys [ks m]
  (reduce flatten-key m ks))

(defn parse-line [l]
  (json/read-str l :key-fn keyword)
  )

(defn flatten-ad [ad]
  (into {} (filter second
                   (-> ad
                       (flatten-key :description)
                       (flatten-key :employer)
                       (flatten-key :occupation)
                       (flatten-key :occupation_group)
                       (flatten-key :occupation_field)
                       (flatten-key :employment_type)
                       (flatten-key :application_details)
                       (flatten-key :workplace_address)
                       (flatten-key :working_hours_type)
                       (flatten-key :salary_type)
                       (flatten-key :duration)
                       (flatten-key :other_old_legacy_attributes)
                       (flatten-key :must_have)
                       (dissoc :nice_to_have)
                       (dissoc :must_to_have)
                       (dissoc :scope_of_work)
                       ))))

(defn numbers-to-string [m]
  (w/walk (fn [[k v]] [k (str v)]) identity m)
  )

(defn build-ad-index [ads-file index]
  (with-open [rdr (clojure.java.io/reader ads-file)]
    (lucene/index! index
                   (mapv #(numbers-to-string (flatten-ad (parse-line %))) (line-seq rdr))
                   {:stored-fields [
                                    :description_text
                                    :occupation_concept_id
                                    :headline
                                    :occupation_label
                                    :id
                                    ]})))

(defn search [text index]
  (lucene/search index {:description_text text}
                 {:results-per-page 3 :hit->doc ld/document->map})
  )


;; (build-ad-index "/home/jafhk/repo/data/conll/pb_2006_2020/2015.jsonl" ad-index-all)
;; (build-ad-index "/home/jafhk/repo/data/conll/pb_2006_2020/2016.jsonl" ad-index-all)
;; (build-ad-index "/home/jafhk/repo/data/conll/pb_2006_2020/2018.jsonl" ad-index-all) ;; error


(defn search-word [text result-per-page]
  (lucene/search ad-index-all {:description_text text}
                 {:results-per-page result-per-page :hit->doc ld/document->map})
  )

(def hotell-search #{"Hotellvärdina" "värd" "ARBETSPLATS" "Hotell" "vinterpalatset" "är" "beläget" "i" "Kiruna" "och" "har" "7" "anställda" "."}
  )

(defn token-reducer [acc element]
  (if (= element ["" ""])
    (-> acc
        (update :sentences conj  (:buff acc))
        (assoc :buff [])
        )
    (update acc :buff conj element)
    )
  )

(defn group-conll-to-sentences [classified-tokens]
  (reduce token-reducer {:buff [] :sentences []} (read-conll))
  )


(def sentences (:sentences (group-conll-to-sentences (read-conll))))


(def test-ad {:doc-id 4824298,
              :score 35.177185,
              :hit
              {:headline "Hotellvärdinna/värd",
               :occupation_concept_id "Zd3B_hjB_3KF",
               :description_text
               "ARBETSPLATS\nHotell vinterpalatset är beläget i Kiruna och har 7 anställda. Vi behöver nu anställa en hotellvärdinna/värd för anställning under sommaren.\n\nARBETSUPPGIFTER:\nDu skall jobba i receptionen, hjälpa till med frukostservering och utföra viss hotellstädning.\n\nERFAREHET:\nTidigare erfarenhet av hotellarbete är en fördel men inget krav. Du behöver ha goda kunskaper i svenska och engelska i tal och skrift för att kunna kommunicera med våra gäster.",
               :occupation_label "Hotellvärd/Hotellvärdinna"}})

(comment
  ;; TODO Fetch ads per sentence. with or query
  ;; TODO Fetch ads with two sentence before and after
  ;; TODO
  )

(defn search-ad-with-sentece [sentence]
  (search-word (set (flatten sentence)))
  )


(defn find-labels-in-text [text s]
  (let [matcher (re-matcher (re-pattern s) text)
        matches ((fn step []
                   (when-let [match (re-find matcher)]
                     (cons {:start-position (.start matcher)
                            :end-position (.end matcher)
                            :matched-string (clojure.string/lower-case match)
                            } (lazy-seq (step))))))
        ]
    matches
    ))


;; acc
#_ {:ads {123 {}}

    :current-ad {:ad {:id 123 :text "banan"}
                 :sentences [["banan"]]
                :annotations [{:type "af-skill" :start-pos 1 :end-pos 5 :matched-string "banan"}]
                 }
    }

;; sentence contains taxonomy type
;; {:token-buffer [["hotel" "title"] ["vardinna" "title"] ]
;;
;;  }

;; (?<=en)\s*hotellvärdinna\s*(?=/)

(comment
  ;; TODO

  "Merge headline with text with a \n to separate the headline from the text"
  "If the token has a class that is not O "
  "Build regexp with the tokens before and after"
  "create an annotation with the matched string."

  "improvment idea
   add more words after
   svenska.*(?=och.*engelska.*i.*tal)"

  )

(defn create-padded-partitions-from-sentence [tokens-with-class-sentence]
  (partition 3 1  [["" "O"]] (cons ["" "O"] tokens-with-class-sentence))
  )

(defn lookbehind [s]
  (if (not (empty? s))
    (str "(<=" s  ")\\s*")
    ""
    )
  )

(defn lookafter [s]
  (if (not (empty? s))
    (str "\\s*(?=" s ")")
    ""
    ))

(defn tokens-to-regexp-reducer [acc element]
  (if (not (= "O" (second (second element))))
    (conj acc (str (lookbehind (ffirst element))  (first (second element)) (lookafter (first (nth  element 2)))))
    acc
    ))

(def test-tokens {:center ["utföra" "af-task"]
      :before
      [["ARBETSUPPGIFTER" "O"]
       [":" "O"]
       ["Du" "O"]
       ["skall" "O"]
       ["jobba" "O"]
       ["i" "O"]
       ["receptionen" "af-task"]
       ["," "O"]
       ["hjälpa" "af-task"]
       ["till" "af-task"]
       ["med" "af-task"]
       ["frukostservering" "af-task"]
       ["och" "af-task"]],
      :after [["viss" "af-task"] ["hotellstädning" "af-task"] ["." "O"]]})

(def regex-char-esc-smap
  (let [esc-chars "(){}[]&^%$#!?*.+/"]
    (zipmap esc-chars
            (map #(str "\\" %) esc-chars))))

(defn escape-regexp [s]
  (->> s (replace regex-char-esc-smap) (reduce str))
  )

(defn token-to-regexp-str [token]
  (escape-regexp (first token))
  )

(defn lookbefore-reducer [acc element]
  (str acc (token-to-regexp-str element) "[\\s\\S]*")
  )

(defn lookafter-reducer [acc element]
  (str acc (token-to-regexp-str element) "[\\s\\S]*")
  )

(defn lookaround-reducer [acc element]
  (str acc (token-to-regexp-str element) "[\\s\\S]*")
  )

(defn create-regexp [{:keys [center before after]}]
  (str
   (if (not (empty? before))
     (str "(?<="
          (clojure.string/join "[\\s\\S]*" (map :regexp before) )
          "[\\s\\S]*)"))

   (:regexp center)

   (if (not (empty? after))
     (str "(?=[\\s\\S]*"
          (clojure.string/join "[\\s\\S]*" (map :regexp after) )
          ")")
     )
   ))

(defn create-regexp-from-tokens [token-with-context]
  {:regexp (create-regexp token-with-context)
   :class  (:class (:center token-with-context))
   }
  )

;; TODO merge consequtive tokens with the same class
;; dont create regexp if not class change
;; add this later. Loop over all annotations and look for consequtive annotation with the same class. and merge them later


(defn partition-tokens-on-class [sentence]
  (partition-by #(identity (second %))  sentence)
  )

(defn merge-tokens-on-class [partitioned-tokens]
  {:regexp (clojure.string/join "[\\s\\S]*" (map #(escape-regexp (first %))   partitioned-tokens) )
   :class (second (first partitioned-tokens))
   }
  )

;; add a buffer wtih the la
(defn sentence-to-regexp [sentence]
  (loop [
         tokens sentence
         before []
         result []
         ]

    (if (empty? tokens)
      result
      (recur
       (rest tokens)
       (conj before (first tokens))
       (if (not (= "O" (:class (first tokens))))
         (conj result (create-regexp-from-tokens {:center (first tokens)
                                                           :before before
                                                           :after (rest tokens)
                                                           }))
         result
         ))))
)


(defn sentece-to-regex-tokens [sentence]
  (sentence-to-regexp
   (map merge-tokens-on-class (partition-tokens-on-class sentence)))
  )


(defn sentece-taxonomy-type [sentence]

  )

(defn sentence-ad-reducer [acc element]
  (let [ad (search-ad-with-sentece element)

        ]
    )
  )

(defn search-ad-with-sentece [sentence]
  (first (search-word (set (flatten sentence)) 1))
  )

(defn find-labels-in-text [text regexp-pattern]
  (let [matcher (re-matcher (re-pattern (:regexp regexp-pattern)) text)
        matches ((fn step []
                   (when-let [match (re-find matcher)]
                     (cons {:start-position (.start matcher)
                            :end-position (.end matcher)
                            :matched-string (clojure.string/lower-case match)
                            :type (:class regexp-pattern)
                            } (lazy-seq (step))))))        ]
    matches
    ))

(defn find-annotations-in-ad [adtext sentence-regex]
  (let [all-text "allADBAFDIGBOAJGBFDIJ"

        ])
  )

(defn get-text-from-ad-hit [ad]
  (str (get-in ad [:hit :headline]) "\n" (get-in ad [:hit :description_text]))
  )


;; Do sentences separately, then merge the annotations with consequtive ad ids
(defn ad-to-annotation-mapper [sentence]
  (let [ad (search-ad-with-sentece sentence)
        sentece-regexp-list (sentece-to-regex-tokens sentence)
        all-text (get-text-from-ad-hit ad)
        ]
    (if (not (empty? sentece-regexp-list))
      (find-labels-in-text all-text (first sentece-regexp-list))
      )
    )
  )
