(ns jobtech.word-to-taxonomy
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.java.shell :as sh]
            [msync.lucene :as lucene]
            [msync.lucene [document :as ld]]
            [msync.lucene.analyzers :as analyzers]
            [clojure.data.json :as json]
            [msync.lucene.indexer :as indexer]
            [iota]
            [clojure.set :as s]
            [clojure.string :as st]
            [jobtech.job-ad-conll-parser :as jobs]
            )
  (:import [java.io BufferedReader StringReader]
           [org.apache.lucene.search PhraseQuery$Builder]
           [org.apache.lucene.index DirectoryReader]
           [ org.apache.lucene.search IndexSearcher]
           [org.apache.lucene.index Term]
           )
  (:gen-class))

;; TODO index all documents in batches
;; search top 1000 documens
;; do extra regexp to see if exact match in text


(def all-ads-file "/home/jafhk/repo/data/conll/pb_2006_2020/all.jsonl")


(def all-taxonomy-occupations-file "resources/all_tax_occupations.txt")
(def all-ontology-occupations-file "resources/all_ont_occupations.txt")

(def all-taxonomy-occupations
  (remove empty? (st/split-lines (slurp all-taxonomy-occupations-file)))
  )
;; 30464


(def all-ontology-occupations
  (remove empty? (st/split-lines (slurp all-ontology-occupations-file)))
  )
;; 30464

(defn get-intersection-ont-and-tax-occ []
  (s/intersection (set all-taxonomy-occupations) (set all-ontology-occupations)))
;; 2675


(defn get-difference-tax-and-ont-occ []
  (s/difference (set all-taxonomy-occupations) (set all-ontology-occupations)))
;; 1242


(defn get-difference-ont-and-tax-occ []
  (s/difference (set all-ontology-occupations) (set all-taxonomy-occupations)))
;; 27789


(defn search-word [text]
  (lucene/search jobs/ad-index-all {:description_text text}
                 {:results-per-page 1000 :hit->doc ld/document->map})
)

(defn regexp-word [word text]
  (if (or (empty? word) (empty? text))
    false
    (re-find (re-pattern (str "(?i)" word)) text)
    )
  )

(defn hit-contains-word? [hit word]
  (or
   (regexp-word word (get-in hit [:hit :description_text]))
   (regexp-word word (get-in hit [:hit :headline]))
   )
  )


(defn get-ads-containing-word [word]
  (let [lucene-ads (search-word word)
        regexp-ads (filter #(hit-contains-word? % word) lucene-ads)
        ]
    regexp-ads
    )
  )

(defn count-occupation-name-fn [acc element]
  (let [id (get-in element [:hit :occupation_concept_id])
        label (get-in element [:hit :occupation_label])
        ]
    (update acc [id label] (fnil inc 0))
    )
  )

(defn get-occupation-name-count-from-ads [ads]
  (reduce count-occupation-name-fn {} ads)
  )

(defn sort-map-by-vals [m]
  (into (sorted-map-by (fn [key1 key2]
                         (compare [(get m key2) key2]
                                  [(get m key1) key1])))
        m))


(defn get-occupation-name-count-by-word [word]
  [word  (sort-map-by-vals
            (get-occupation-name-count-from-ads
             (get-ads-containing-word word)))]
  )

(defn into-row [occupation-count-result]
  (cons (first occupation-count-result ) (flatten (into [] (second occupation-count-result)))        )
  )



(defn all-occupation-name-count-ontology []
  (doall (map #(
                into-row (get-occupation-name-count-by-word %)
                ) (filter identity (sort (get-difference-ont-and-tax-occ)))))
  )


(defn write-to-csv [fun]
  (with-open [writer (io/writer "ont-occupations-3.csv")]
    (csv/write-csv writer
                   (fun)
                   :separator \;
                   ))
  )


;; (sh/sh "grep" "vinterpalatset" jobads-file)

;; ;; (DirectoryReader/open (:directory jobs/ad-index-disk2))


(defn get-search-index []
  (let [dr (DirectoryReader/open (:directory jobs/ad-index-disk2))
        searcher  (new IndexSearcher dr)
        ]
    searcher
    )
  )

(defn build-exact-phrase-query [text]
  (let [query (new PhraseQuery$Builder)]
    (doseq [word (st/split text #" ")]
      (.add query (new Term "contents" word)))
    (.build query)
    )
  )
