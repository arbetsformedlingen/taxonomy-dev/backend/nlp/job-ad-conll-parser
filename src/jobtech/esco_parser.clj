(ns jobtech.esco-parser
  (:require [clojure.data.csv :as csv]
            [clojure.data.json :as json]
            [clj-http.client :as client]
            )
  (:gen-class))


(defn get-esco-occupations-from-api []
  (client/get "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql"
              {:as :json-strict
               :accept :json
               :query-params {"query" "query MyQuery {
  concepts(type: \"occupation-name\") {
    id
    preferred_label
    type
    exact_match{
      id
      esco_uri
      preferred_label
      type
    }
    broad_match {
      id
      esco_uri
      preferred_label
      type
    }
    close_match {
      id
      esco_uri
      preferred_label
      type
    }
    narrow_match{
      id
      esco_uri
      preferred_label
      type
    }
    broader(type: \"isco-level-4\"){
      id
      preferred_label
      type
      esco_uri
    }
  }
}
"}
               }
              )
  )


(def test-data [{:id "ghs4_JXU_BYt",
                 :preferred_label "Planeringsarkitekt/Fysisk planerare",
                 :type "occupation-name",
                 :exact_match [],
                 :broad_match
                 [{:id "XAcf_SQ9_fJw",
                   :esco_uri "http://data.europa.eu/esco/isco/C2164",
                   :preferred_label "Planeringsarkitekter m.fl.",
                   :type "isco-level-4"}],
                 :close_match [],
                 :narrow_match [],
                 :broader
                 [{:id "XAcf_SQ9_fJw",
                   :preferred_label "Planeringsarkitekter m.fl.",
                   :type "isco-level-4",
                   :esco_uri "http://data.europa.eu/esco/isco/C2164"}]}

                {:id "GPNi_fJR_B2B",
                 :preferred_label "Inredningsdesigner",
                 :type "occupation-name",
                 :exact_match
                 [{:id "TYat_S2W_WHT",
                   :esco_uri
                   "http://data.europa.eu/esco/occupation/73e776fb-4d99-4031-bad4-7716f121155d",
                   :preferred_label "inredare",
                   :type "esco-occupation"}
                  {:id "iCXb_uUw_Fye",
                   :esco_uri
                   "http://data.europa.eu/esco/occupation/773e5ea3-235b-4e72-afc5-290dd841eed8",
                   :preferred_label "inredningsplanerare",
                   :type "esco-occupation"}],
                 :broad_match [],
                 :close_match [],
                 :narrow_match
                 [{:id "Xr26_hzA_aLA",
                   :esco_uri
                   "http://data.europa.eu/esco/occupation/98af7387-3974-457a-aa57-5397d1ea3949",
                   :preferred_label "växtinredare",
                   :type "esco-occupation"}],
                 :broader
                 [{:id "iw8P_7PW_2By",
                   :preferred_label "Inredare och dekoratörer",
                   :type "isco-level-4",
                   :esco_uri "http://data.europa.eu/esco/isco/C3432"}]}

                {:id "JfBL_7pn_Ky3",
                 :preferred_label "Forskningslaborant, kemi",
                 :type "occupation-name",
                 :exact_match [],
                 :broad_match
                 [{:id "xUcL_EcU_8xr",
                   :esco_uri
                   "http://data.europa.eu/esco/occupation/4fef1907-cf82-413b-841a-02ef57e299a6",
                   :preferred_label "forskningslaborant",
                   :type "esco-occupation"}],
                 :close_match [],
                 :narrow_match [],
                 :broader
                 [{:id "ZvhE_zoe_1st",
                   :preferred_label "Laboratorieingenjörer",
                   :type "isco-level-4",
                   :esco_uri "http://data.europa.eu/esco/isco/C3111"}]}

                ])


(def test-data-2 {:id "yJHw_ezK_66B",
                    :preferred_label "Maskinoperatör, hygienteknik",
                    :type "occupation-name",
                    :exact_match [],
                    :broad_match [],
                    :close_match [],
                    :narrow_match
                    [{:id "zBjB_Hek_Y8F",
                      :esco_uri
                      "http://data.europa.eu/esco/occupation/29a36f21-da45-4a01-b015-cac4445e5025",
                      :preferred_label "maskinoperatör, hygienteknik",
                      :type "esco-occupation"}
                     {:id "qXd6_zTn_PkW",
                      :esco_uri
                      "http://data.europa.eu/esco/occupation/3a67544c-919f-4051-b812-c08e69eec3fd",
                      :preferred_label "maskinoperatör, tvålpressning",
                      :type "esco-occupation"}
                     {:id "1rQi_p7p_RHq",
                      :esco_uri
                      "http://data.europa.eu/esco/occupation/39e06c20-d262-44f9-a6a8-a0f3ba9e887c",
                      :preferred_label "tvålmaskinskötare",
                      :type "esco-occupation"}
                     {:id "bmhZ_aPo_jK9",
                      :esco_uri
                      "http://data.europa.eu/esco/occupation/c7c3f05b-319b-4aa7-891e-78106f875de2",
                      :preferred_label "maskinoperatör, tvåltvättmedel",
                      :type "esco-occupation"}
                     {:id "4VGb_MxS_i8H",
                      :esco_uri
                      "http://data.europa.eu/esco/occupation/34c191ec-e3e3-4746-ae10-7ff4306a2794",
                      :preferred_label
                      "maskinoperatör, hygienteknik, tvåltillverkning",
                      :type "esco-occupation"}],
                    :broader
                    [{:id "bNAA_RQi_rjm",
                      :preferred_label
                      "Maskinoperatörer, kemiska och farmaceutiska produkter",
                      :type "isco-level-4",
                      :esco_uri "http://data.europa.eu/esco/isco/C8131"}]})

(defn parse-esco-occupations-concepts [response]
  (get-in response [:body :data :concepts])
  )

(defn convert-concept-to-esco-ids  [concept-with-relations]
  (let [concept concept-with-relations
        broad-match   (:broad_match concept)
        close-match   (:close_match concept)
        exact-match   (:exact_match concept)
        narrow-match  (:narrow_match concept)

        broad-match-count  (count broad-match)
        close-match-count  (count close-match)
        exact-match-count  (count exact-match)
        narrow-match-count (count narrow-match)

        taxonomy-concept (select-keys concept [:id :preferred_label :type])

        eures-concepts (if (< 1 (+ broad-match-count close-match-count exact-match-count narrow-match-count))
                         (if (or (= 1 exact-match-count) (= 2 exact-match-count) (= 3 exact-match-count))
                           exact-match
                           (if (< 3 (+ broad-match-count close-match-count exact-match-count narrow-match-count))
                             (:broader concept)
                             (if (not-empty narrow-match)
                               (:broader concept)
                               (concat broad-match close-match exact-match narrow-match)
                               )))
                         (concat broad-match close-match exact-match narrow-match)
                         )
        ]
    {taxonomy-concept eures-concepts}
    )
  )


(defn apply-logic-from-taxonomy-to-esco [all-concepts]
  (map convert-concept-to-esco-ids  all-concepts)
  )

(defn converted-data []
  (apply-logic-from-taxonomy-to-esco (parse-esco-occupations-concepts (get-esco-occupations-from-api)))
  )

(defn pretty-spit
  [file-name collection]
  (spit (java.io.File. file-name) (with-out-str (clojure.pprint/write collection :dispatch clojure.pprint/code-dispatch))))

(defn get-specific [id]
  (filter #(= id (second (first (first (first %)))) )  (converted-data))
  )
